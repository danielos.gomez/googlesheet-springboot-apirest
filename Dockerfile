FROM openjdk:8
EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/copa-0.0.1-SNAPSHOT.jar /app/copa-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/app/copa-0.0.1-SNAPSHOT.jar"]