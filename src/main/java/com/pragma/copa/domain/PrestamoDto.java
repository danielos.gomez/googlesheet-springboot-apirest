package com.pragma.copa.domain;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class PrestamoDto {
    String name,document,pc,desc;
}
