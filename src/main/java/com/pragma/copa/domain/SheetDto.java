package com.pragma.copa.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SheetDto {
    String sheetId, sheetName, rangIni,  ranFin , cell;
}
