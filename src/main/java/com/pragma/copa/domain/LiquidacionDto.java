package com.pragma.copa.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
//@Builder
public class LiquidacionDto {
    String name,document;
    BigDecimal liquidation;

}
