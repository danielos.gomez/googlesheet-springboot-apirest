package com.pragma.copa.controllers.liquidaciones;

import com.pragma.copa.domain.LiquidacionDto;
import com.pragma.copa.services.Factory;
import com.pragma.copa.services.GoogleSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@RestController
@RequestMapping("/liquidaciones")

public class LiquidacionesController {

    @Autowired
    GoogleSheetService service;
    @Autowired
    Factory factory;


    @GetMapping(value = "/{mes}")
    public ResponseEntity<List<LiquidacionDto>> getAll (@PathVariable("mes") String mes) throws GeneralSecurityException, IOException {
        return new ResponseEntity<>(service.getListLiquidacion(factory.getConfigSheet("liquidacion",mes),""),HttpStatus.OK);
    }

    @GetMapping(value = "/{mes}/{cedula}")
    public ResponseEntity<List<LiquidacionDto>> getOne (@PathVariable("mes") String mes, @PathVariable("cedula") String cedula) throws GeneralSecurityException, IOException {
        return new ResponseEntity<>(service.getListLiquidacion(factory.getConfigSheet("liquidacion",mes),cedula),HttpStatus.OK);
    }


    @PostMapping(value = "/{mes}/{cedula}/{value}")
    public ResponseEntity<HttpStatus> Update (@PathVariable("mes") String mes, @PathVariable("cedula") String cedula , @PathVariable("value") String value) throws Exception {
        service.updateData(factory.getConfigSheet("updateEstado",mes),cedula,value);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
