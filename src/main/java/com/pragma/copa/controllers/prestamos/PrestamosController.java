package com.pragma.copa.controllers.prestamos;

import com.pragma.copa.domain.PrestamoDto;
import com.pragma.copa.services.Factory;
import com.pragma.copa.services.GoogleSheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@RestController
@RequestMapping("/prestamos")

public class PrestamosController {

    @Autowired
    GoogleSheetService service;
    @Autowired
    Factory factory;


    @GetMapping()
    public ResponseEntity<List<PrestamoDto>> getAll () throws GeneralSecurityException, IOException {
        return new ResponseEntity<>(service.getListPrestamo(factory.getConfigSheet("prestamo","")),HttpStatus.OK);
    }
}
