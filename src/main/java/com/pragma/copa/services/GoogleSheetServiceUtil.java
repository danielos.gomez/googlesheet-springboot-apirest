package com.pragma.copa.services;

import com.pragma.copa.domain.LiquidacionDto;
import com.pragma.copa.domain.PrestamoDto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GoogleSheetServiceUtil {


    public static List<LiquidacionDto> toLiquidationDtoList(List<List<Object>> sheet){
        List<LiquidacionDto> liquidationList = new ArrayList<>();
        for (List row : sheet) {

            LiquidacionDto liquidacionDto = new LiquidacionDto();
            liquidacionDto.setName(String.valueOf(row.get(0)));
            liquidacionDto.setDocument(String.valueOf(row.get(1)));
            liquidacionDto.setLiquidation(new BigDecimal(String.valueOf(row.get(2))));
            liquidationList.add(liquidacionDto);
        }
        return  liquidationList;
    }

    public static List<LiquidacionDto> toLiquidationDtoList(List<List<Object>> sheet , String cedula){
        List<LiquidacionDto> liquidationList = new ArrayList<>();
        LiquidacionDto liquidacionDto = new LiquidacionDto();

        sheet.stream()
                .filter(row -> Objects.equals(String.valueOf(row.get(1)), cedula))
                .forEach(result-> {
                            assert false;
                            liquidacionDto.setName(String.valueOf(result.get(0)));
                            liquidacionDto.setDocument(String.valueOf(result.get(1)));
                            liquidacionDto.setLiquidation(new BigDecimal(String.valueOf(result.get(2))));
                        }
                );

        liquidationList.add(liquidacionDto);

        return liquidationList;

    }

    public static List<PrestamoDto> toPrestamoDtoList(List<List<Object>> sheet){
        List<PrestamoDto> listPrestamo = new ArrayList<>();
        for (List row : sheet) {
            PrestamoDto prestamoDto =
                    PrestamoDto.builder()
                            .name(String.valueOf(row.get(0)))
                            .document(String.valueOf(row.get(1)))
                            .pc(String.valueOf(row.get(2)))
                            .desc(String.valueOf(row.get(3)))
                            .build();
            listPrestamo.add(prestamoDto);
        }
        return  listPrestamo;
    }
}
