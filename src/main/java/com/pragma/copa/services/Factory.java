package com.pragma.copa.services;

import com.pragma.copa.domain.SheetDto;
import org.springframework.stereotype.Service;

@Service
public class Factory {

    public SheetDto getConfigSheet(String type, String mes){
        SheetDto sheetDto = null;
         if(type=="prestamo"){
             sheetDto = SheetDto.builder()
                    .sheetId("1lRwDP_HlEHgdAFqdz8lBaHL9ozsKptP16IvEfVGvSw0")
                    .sheetName("inventario")
                    .rangIni("A2")
                    .ranFin("D12").build();
        }

        if(type=="liquidacion"){
            return SheetDto.builder()
                    .sheetId("1FqNvAzlR5uso45Kbt7afYqG1-GFr7gXeY23IAoW8SG4")
                    .sheetName(mes)
                    .rangIni("A2")
                    .ranFin("C12").build();
        }

        if(type=="updateEstado"){
            return SheetDto.builder()
                    .sheetId("1FqNvAzlR5uso45Kbt7afYqG1-GFr7gXeY23IAoW8SG4")
                    .sheetName(mes)
                    .cell("D")
                    .rangIni("A2")
                    .ranFin("C12").build();
        }

        return sheetDto;
    }
}
